package test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import main.dao.SocialMediaDAO;
import main.factory.Factory;
import main.pojo.Post;
import main.service.SocialMediaActivities;

public class SocialMediaServiceTest {

	private final SocialMediaDAO collection = SocialMediaDAO.getSocialMediaDAO();
//	private final SocialMediaDAO collection = new SocialMediaDAO();
	
	Factory factory = null;
	SocialMediaActivities activities = null;

	@Before
	public void createPostAndFollowUsers() {

		factory = new Factory();
		activities = factory.getSocialMediaActivities();

		activities.createPost("Sagar", "SagarPost1", "Message1");
		activities.createPost("Sagar", "SagarPost2", "Message2");
		activities.createPost("Raj", "RajPost1", "Message1");
		activities.createPost("Raj", "RajPost2", "Message2");
		activities.createPost("Sagar", "SagarPost3", "Message3");

		List<Post> i = collection.getUserPosts().get("sagar");
		
		activities.follow("Sagar", "Raj");
		activities.follow("Ashish", "Raj");
		activities.follow("Mayur", "Sagar");
		activities.follow("Sagar", "Mayur");
		activities.follow("Mayur", "Ashish");
		activities.follow("Ashish", "Sagar");
		activities.follow("Ashish", "Mayur");
		activities.follow("Sagar", "Ashish");
		activities.follow("Mayur", "Raj");
		activities.follow("Raj", "Sagar");
		
		List<Post> j = collection.getUserPosts().get("sagar");
		System.out.println("");
	}

	@After
	public void emptyList() {
//		collection.getUserPosts().clear();
//		collection.getFollowers().clear();

		factory = null;
		activities = null;
	}

	@Test
	public void userPosts_Test() {
		List<Post> j = collection.getUserPosts().get("sagar");
		List<String> getNewsFeed = activities.getNewsFeed("Sagar");

		List<Post> i = collection.getUserPosts().get("sagar");
		
		assertEquals("SagarPost3", getNewsFeed.get(0));
		assertEquals("RajPost2", getNewsFeed.get(1));
		assertEquals("RajPost1", getNewsFeed.get(2));
		assertEquals("SagarPost2", getNewsFeed.get(3));
		assertEquals("SagarPost1", getNewsFeed.get(4));
	}

	@Test
	public void userPostsAfterUnfollowingUser_Test() {
		activities.unfollow("Raj", "Sagar");
		List<String> getNewsFeed = activities.getNewsFeed("Sagar");

		assertEquals("SagarPost3", getNewsFeed.get(0));
		assertEquals("SagarPost2", getNewsFeed.get(1));
		assertEquals("SagarPost1", getNewsFeed.get(2));
	}
}