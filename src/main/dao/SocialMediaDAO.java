package main.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import main.pojo.Post;

/**
 * This file will work as a Database. It will store all the User's Post and
 * their followers
 * 
 * @author skhanke
 */
public class SocialMediaDAO {

	private static final SocialMediaDAO collection = new SocialMediaDAO();

	// Making this file as Singleton by creating a private Constructor so the
	// multiple instances of this file should not be created
	private SocialMediaDAO() {

	}

	// This method will always return the same instance of this class.
	public static SocialMediaDAO getSocialMediaDAO() {
		return collection;
	}

	private Map<String, List<Post>> userPosts = new HashMap<>();

	private Map<String, List<String>> followers = new HashMap<>();

	public Map<String, List<Post>> getUserPosts() {
		return userPosts;
	}

	public void setUserPosts(Map<String, List<Post>> userPosts) {
		this.userPosts = userPosts;
	}

	public Map<String, List<String>> getFollowers() {
		return followers;
	}

	public void setFollowers(Map<String, List<String>> followers) {
		this.followers = followers;
	}

}
