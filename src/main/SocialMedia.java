package main;

import java.util.List;

import main.factory.Factory;
import main.service.SocialMediaActivities;

public class SocialMedia {

	private static final Factory factory = new Factory();

	public static void main(String[] args) {

		SocialMediaActivities operation = factory.getSocialMediaActivities();

		operation.createPost("Sagar", "SagarPost1", "Message1");
		operation.createPost("Sagar", "SagarPost2", "Message2");
		operation.createPost("Raj", "RajPost1", "Message1");
		operation.createPost("Raj", "RajPost2", "Message2");
		operation.createPost("Sagar", "SagarPost3", "Message3");

		operation.follow("Sagar", "Raj");
		operation.follow("Ashish", "Raj");
		operation.follow("Mayur", "Sagar");
		operation.follow("Sagar", "Mayur");
		operation.follow("Mayur", "Ashish");
		operation.follow("Ashish", "Sagar");
		operation.follow("Ashish", "Mayur");
		operation.follow("Sagar", "Ashish");
		operation.follow("Mayur", "Raj");
		operation.follow("Raj", "Sagar");

		List<String> getNewsFeed = operation.getNewsFeed("Sagar");

		if (getNewsFeed != null && !getNewsFeed.isEmpty()) {
			// Printing all the posts
			getNewsFeed.forEach(System.out::println);
		}
	}

}
