package main.factory;

import main.serviceImpl.SocialMediaActivitiesImpl;

/**
 * This is a factory class. It job is to create an instance of a class based on
 * user request.
 * 
 * @author skhanke
 */
public class Factory {

	public SocialMediaActivitiesImpl getSocialMediaActivities() {
		return new SocialMediaActivitiesImpl();
	}
}
