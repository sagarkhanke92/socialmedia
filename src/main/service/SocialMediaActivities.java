package main.service;

import java.util.List;

/**
 * This is an interface with the method Signature for the requiremnt. It will
 * hide the actual implementation from the outside.
 * 
 * @author skhanke
 */
public interface SocialMediaActivities {
	public void createPost(String userId, String postId, String content);

	public List<String> getNewsFeed(String userId);

	void follow(String followerId, String followeeId);

	void unfollow(String followerId, String followeeId);
}
