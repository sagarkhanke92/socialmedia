package main.serviceImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import main.dao.SocialMediaDAO;
import main.dao.User;
import main.pojo.Post;
import main.service.SocialMediaActivities;

/**
 * This is the implementation class. It provides the implementation for the
 * requirement.
 * 
 * @author skhanke
 */
public class SocialMediaActivitiesImpl implements SocialMediaActivities {

	private final SocialMediaDAO collection = SocialMediaDAO.getSocialMediaDAO();
//	private final SocialMediaDAO collection = new SocialMediaDAO();

	/**
	 * It will create a new post for the user.
	 * 
	 * @param userId
	 * @param postId
	 * @param content
	 */
	@Override
	public void createPost(String userId, String postId, String content) {
		User user = new User();
		user.setUserId(userId);

		Post post = new Post();
		post.setPostId(postId);
		post.setContent(content);
		post.setLocalDateTime(LocalDateTime.now());

		// If the post for this User already exist then adding new posts to it
		// else this is the user's 1st post.
		List<Post> posts = collection.getUserPosts().containsKey(userId.toLowerCase())
				? collection.getUserPosts().get(userId.toLowerCase()) : new ArrayList<>();
		posts.add(post);

		// Stopping the thread for 1 second. This will help while sorting Users
		// based on time.
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		collection.getUserPosts().put(userId.toLowerCase(), posts);
	}

	/**
	 * It will follow the user.
	 * 
	 * @param followerId
	 * @param followeeId
	 */
	@Override
	public void follow(String followerId, String followeeId) {

		List<String> followerList = collection.getFollowers().containsKey(followeeId.toLowerCase())
				? collection.getFollowers().get(followeeId.toLowerCase()) : new ArrayList<>();

		followerList.add(followerId.toLowerCase());

		collection.getFollowers().put(followeeId.toLowerCase(), followerList);

		System.out.println(followerId + " is now following " + followeeId + ".");
	}

	/**
	 * It will unfollow the user.
	 * 
	 * @param followerId
	 * @param followeeId
	 */
	@Override
	public void unfollow(String followerId, String followeeId) {

		List<String> followeeList = collection.getFollowers().get(followeeId.toLowerCase());

		Set<String> uniqueFollowees = new HashSet<>(followeeList);
		
		uniqueFollowees.remove(followerId.toLowerCase());

		collection.getFollowers().put(followeeId.toLowerCase(), new ArrayList<>(uniqueFollowees));
		
		System.out.println(followerId + " is not following " + followeeId + " any more.");
	}

	/**
	 * It will return all the posts for the user if there are any.
	 * 
	 * @param userId
	 * @return
	 */
	@Override
	public List<String> getNewsFeed(String userId) {

		final List<Post> j = collection.getUserPosts().get("sagar");
		
		final List<Post> posts = collection.getUserPosts().get(userId.toLowerCase());

		List<String> userIds = collection.getFollowers().get(userId.toLowerCase());

		getFollowersPost(posts, userIds);

		if (posts == null || posts.isEmpty()) {
			System.out.println("No Posts available for User: " + userId);
			return null;
		} else {
			System.out.println("Posts available for User: " + userId);
			// Sorting the User's Posts based on time in Descending Order.
			posts.sort(Comparator.comparing(Post::getLocalDateTime).reversed());

			int size = posts.size() > 20 ? 20 : posts.size();
			return posts.subList(0, size).stream().map(Post::getPostId).collect(Collectors.toList());
		}
	}

	/**
	 * It will return the follower's list
	 * 
	 * @param posts
	 * @param userIds
	 * @return
	 */
	private List<Post> getFollowersPost(List<Post> posts, List<String> userIds) {
		
		List<Post> j = collection.getUserPosts().get("sagar");
		
		if (userIds != null && !userIds.isEmpty()) {
			for (String followerUserid : userIds) {

				List<Post> followersPost = collection.getUserPosts().get(followerUserid);

				if (followersPost != null && !followersPost.isEmpty()) {
					if (posts == null) {
						posts = new ArrayList<>();
					}
					posts.addAll(followersPost);
				}
			}
		}
		return posts;
	}
}
